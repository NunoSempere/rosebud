# Requires:
# sudo cp rose.desktop /usr/share/applications

xdg-mime default rose.desktop x-scheme-handler/https
xdg-mime default rose.desktop x-scheme-handler/http

# This is useful e.g., for setting how to open
# links in vim when pressing gx.
