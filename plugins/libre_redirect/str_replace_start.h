#ifndef STR_REPLACE_H_
#define STR_REPLACE_H_

int str_replace_start(const char* string, const char* target, const char* replacement, char* output);

#endif
